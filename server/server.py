import socket
import multiprocessing
from bot import bot_process

if __name__ == '__main__':

    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv_sock.bind(('SERVER_IP', SERVER_PORT))
    serv_sock.listen(1)

    queue = multiprocessing.Queue()
    multiprocessing.Process(target=bot_process, name="bot", args=(queue,)).start()

    while True:
        client_sock, client_addr = serv_sock.accept()
        print('Connected by', client_addr)

        while True:
            data = queue.get().encode('utf-8')

            client_sock.sendall(data)

        client_sock.close()
