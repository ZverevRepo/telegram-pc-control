def bot_process(q):
    import telebot

    ADMIN = YOUR_ID
    bot = telebot.TeleBot('TOKEN_BOT')

    @bot.message_handler(commands=['start'])
    def start_command(message):
        if message.chat.id == ADMIN:
            bot.send_message(message.chat.id, 'Ты админ')
        else:
            bot.send_message(message.chat.id, 'Ты не админ')

    @bot.message_handler(commands=['help'])
    def help_command(message):
        if message.chat.id == ADMIN:
            bot.send_message(message.chat.id, 'Админу нужна помощь?')
        else:
            bot.send_message(message.chat.id, 'Ты не админ')

    @bot.message_handler(commands=['hi'])
    def hi_command(message):
        if message.chat.id == ADMIN:
            q.put("hi")
        else:
            bot.send_message(message.chat.id, 'Ты не админ')

    @bot.message_handler(commands=['test'])
    def hi_command(message):
        if message.chat.id == ADMIN:
            q.put("test")
        else:
            bot.send_message(message.chat.id, 'Ты не админ')

    bot.polling(none_stop=True, interval=0)
