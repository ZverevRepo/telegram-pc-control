import socket

client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_sock.connect(('SERVER_IP', SERVER_PORT))
while True:
    data = client_sock.recv(1024)
    print('Received', data.decode('utf-8'))
